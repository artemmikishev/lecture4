package ru.sbrf.edu;


import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class CustomArrayTest {
    CustomArrayImpl<String> list = new CustomArrayImpl<String>();

    /**
     * Тестируем метод @size().
     */
    @Test
    public void sizeTest() {
        Assert.assertEquals(list.size(), 0);
    }

    /**
     * Тестируем метод @add.
     */
    @Test
    public void addTest(){
        Assert.assertTrue(list.add("Hello"));
    }

    /**
     * Позитивный сценарий для метода @isEmpty.
     */
    @Test
    public void isEmptyTestPositive(){
        Assert.assertTrue(list.isEmpty());
    }
    /**
     * Негативный сценарий для метода @isEmpty.
     */
    @Test
    public void isEmptyTestNegative(){
        list.add("Hello");
        Assert.assertFalse(list.isEmpty());
    }

    /**
     * Тестируем метод @addAll для массива.
     */
    @Test
    public void addAllTestArray(){
        String[] testStrings = {"Say", "Hello", "My", "Friend"};
        Assert.assertTrue(list.addAll(testStrings));
    }

    /**
     * Тестируем метод @addAll для коллекции.
     */
    @Test
    public void addAllTestCollection(){
        ArrayList<String> testStrings = new ArrayList<String>();
        testStrings.add("Say");
        testStrings.add("Hello");
        testStrings.add("My");
        testStrings.add("Friend");
        Assert.assertTrue(list.addAll(testStrings));
    }

    /**
     * Тестируем метод @addAll для массива с добавлением по индексу.
     */
    @Test
    public void addAllTestArrayIndex(){
        String[] testStrings = {"Say", "Hello", "My", "Friend"};
        list.add("0");
        list.add("1");
        list.add("2");
        list.add("3");
        Assert.assertTrue(list.addAll(2, testStrings));
    }

    /**
     * Тест для метода get.
     */
    @Test
    public void getTest(){
        list.add("0");
        list.add("1");
        list.add("2");
        list.add("3");
        Assert.assertEquals("3", list.get(3));
    }

    /**
     * Тест для метода @set.
     */
    @Test
    public void setTest(){
        list.add("0");
        list.add("1");
        list.add("2");
        list.add("3");
        Assert.assertEquals("3", list.set(3, "5"));
        Assert.assertEquals("5", list.get(3));
    }
    /**
     * Тест для метода @remove, void метод, по индексу элемента.
     */
    @Test
    public void removeTestVoid(){
        list.add("Say");
        list.add("Hello");
        list.add("My");
        list.add("Friend");
        int size = 4;
        Assert.assertEquals(list.size(), size);
        list.remove(2);
        Assert.assertEquals(list.size(), size - 1);
    }
    /**
     * Тест для метода @remove по объекту удаления.
     */
    @Test
    public void removeTestBoolean(){
        list.add("Say");
        list.add("Hello");
        list.add("My");
        list.add("Friend");
        int size = 4;
        Assert.assertEquals(list.size(), size);
        Assert.assertTrue(list.remove("My"));
        Assert.assertEquals(list.size(), size - 1);
    }
    /**
     * Тест для метода @indexOf.
     */
    @Test
    public void indexOfTest(){
        list.add("Say");
        list.add("Hello");
        list.add("My");
        list.add("Friend");
        Assert.assertEquals(0, list.indexOf("Say"));
        Assert.assertEquals(1, list.indexOf("Hello"));
    }

    /**
     * Тест для метода @contains.
     */
    @Test
    public void containsTest(){
        list.add("Say");
        list.add("Hello");
        list.add("My");
        list.add("Friend");
        Assert.assertTrue(list.contains("My"));
    }

    /**
     * Тест метода @GetCapacity.
     * По умолчанию - 10.
     */
    @Test
    public void getCapacityTest(){
        list.add("Say");
        list.add("Hello");
        list.add("My");
        list.add("Friend");
        Assert.assertEquals(10, list.getCapacity());
    }

    /**
     * Тест метода @reverse.
     */
    @Test
    public void reverseTest(){
        list.add("Say");
        list.add("Hello");
        list.add("My");
        list.add("Friend");
        list.reverse();
        Assert.assertEquals(list.get(0), "Friend");
        Assert.assertEquals(list.get(1), "My");
        Assert.assertEquals(list.get(2), "Hello");
        Assert.assertEquals(list.get(3), "Say");
    }

    /**
     * Тест метода @toArray.
     */
    @Test
    public void toArrayTest(){
        list.add("Say");
        list.add("Hello");
        list.add("My");
        list.add("Friend");
        Object[] answer = list.toArray();
        for (int i = 0; i < list.size(); i++){
            Assert.assertEquals(answer[i], list.get(i));
        }
    }

    /**
     * Тест метода @ensureCapacity.
     */
    @Test
    public void ensureCapacity(){
        int capacity = 15000;
        list.add("Say");
        list.add("Hello");
        list.add("My");
        list.add("Friend");
        list.ensureCapacity(capacity);
        Assert.assertEquals(capacity, list.getCapacity());
        Assert.assertEquals(list.size(), 4);
    }

    /**
     * Тест метода @toString.
     */
    @Test
    public void tiStringTest(){
        list.add("Say");
        list.add("Hello");
        list.add("My");
        list.add("Friend");
        Assert.assertEquals("Say Hello My Friend ", list.toString());
    }
/**
 * Дальше будут тесты на выбрасывание ошибок.
 */
    /**
     * Тест на добавление пустой коллекции, метод addAll должен вкинуть исключение IllegalArgumentException.
     */
    @Test(expected = IllegalArgumentException.class)
    public void addAllTestWithException(){
        ArrayList<String> nullString = null;
        list.addAll(nullString);
    }
    /**
     * Тест на добавление пустого массива, метод addAll должен вкинуть исключение IllegalArgumentException.
     */
    @Test(expected = IllegalArgumentException.class)
    public void addAllTestWithExceptionArray(){
        String[] nullString = null;
        list.addAll(nullString);
    }
    /**
     * Тест на добавление пустой коллекции по индексу, метод addAll должен вкинуть исключение IllegalArgumentException.
     */
    @Test(expected = IllegalArgumentException.class)
    public void addAllTestWithExceptionIndexCollection(){
        String[] nullString = null;
        list.add("say");
        list.add("say");
        list.add("say");
        list.add("say");
        list.addAll(3, nullString);
    }
    /**
     * Тест на добавление пустой коллекции по индексу, метод addAll должен вкинуть исключение ArrayIndexOutOfBoundsException.
     */
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addAllTestWithExceptionIndexCollectionOut(){
        String[] testStrings = {"Say", "Hello"};
        list.add("say");
        list.add("say");
        list.add("say");
        list.add("say");
        list.addAll(99, testStrings);
    }

    /**
     * Тест метода @get на ошибку ArrayIndexOutOfBoundsException.
     */
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getTestException(){
        list.add("say");
        list.add("say");
        list.add("say");
        list.add("say");
        list.get(4);
    }

    /**
     * Тест метода @set на ошибку ArrayIndexOutOfBoundsException
     */
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void setException(){
        list.add("say");
        list.add("say");
        list.add("say");
        list.add("say");
        list.set(4, "say");
    }

    /**
     * Тест метода @remove на ошибку ArrayIndexOutOfBoundsException
     */
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeException(){
        list.add("say");
        list.add("say");
        list.add("say");
        list.add("say");
        list.set(4, "say");
    }
}
