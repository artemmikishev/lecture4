package ru.sbrf.edu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class CustomArrayImpl<T> implements CustomArray<T> {

    private static final int DEFAULT_CAPACITY = 10;
    private static final Object[] EMPTY_ELEMENTDATA = {};
    private static final Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};
    private Object[] elementData;
    private int size;
    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;


    public CustomArrayImpl(){
        this.elementData = DEFAULTCAPACITY_EMPTY_ELEMENTDATA;

    }

    public CustomArrayImpl(int initialCapacity){
        if (initialCapacity > 0) {
            this.elementData = new Object[initialCapacity];
        } else if (initialCapacity == 0) {
            this.elementData = EMPTY_ELEMENTDATA;
        } else {
            throw new IllegalArgumentException("Illegal Capacity: "+
                    initialCapacity);
        }
    }

    public CustomArrayImpl(Collection<? extends T> anotherList){
        Object[] anotherArray = anotherList.toArray();
        if ((size = anotherArray.length) != 0) {
            elementData = Arrays.copyOf(anotherArray, size, Object[].class);

        } else {
            // replace with empty array.
            elementData = EMPTY_ELEMENTDATA;
        }
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean add(T item) throws IllegalArgumentException{
        ensureCapacityInternal(size + 1);
        elementData[size] = item;
        ++size;
        return true;
    }

    public boolean addAll(T[] items) {
        if (items == null){
            throw new IllegalArgumentException("Items is null");
        }
        int numNew = items.length;
        ensureCapacityInternal(size + numNew);  // Increments modCount
        System.arraycopy(items, 0, elementData, size, numNew);
        size += numNew;
        return numNew != 0;
    }

    public boolean addAll(Collection<T> items) {
        if (items == null){
            throw new IllegalArgumentException("Items is null");
        }
        Object[] a = items.toArray();
        int numNew = a.length;
        ensureCapacityInternal(size + numNew);  // Increments modCount
        System.arraycopy(a, 0, elementData, size, numNew);
        size += numNew;
        return numNew != 0;
    }

    public boolean addAll(int index, T[] items) {
        rangeCheckForAdd(index);
        if (items == null){
            throw new IllegalArgumentException("Items is null");
        }
        int numNew = items.length;
        ensureCapacityInternal(size + numNew);

        int numMoved = size - index;
        if (numMoved > 0)
            System.arraycopy(elementData, index, elementData, index + numNew,
                    numMoved);

        System.arraycopy(items, 0, elementData, index, numNew);
        size += numNew;
        return numNew != 0;
    }

    public T get(int index) {
        rangeCheck(index);
        return (T)elementData[index];
    }

    public T set(int index, T item) {
        rangeCheck(index);

        T oldValue = (T) elementData[index];
        elementData[index] = item;
        return oldValue;
    }

    public void remove(int index) {
        rangeCheck(index);
        elementData[index] = null;
        int sourceArrayElementToCopyCount = size - index - 1;
        if (sourceArrayElementToCopyCount > 0) {
            System.arraycopy(elementData, index + 1, elementData, index, sourceArrayElementToCopyCount);
            elementData[--size] = null;

        }
    }


    public boolean remove(T item) {
        if (item == null) {
            for (int index = 0; index < size; index++)
                if (elementData[index] == null) {
                    fastRemove(index);
                    return true;
                }
        } else {
            for (int index = 0; index < size; index++)
                if (item.equals(elementData[index])) {
                    fastRemove(index);
                    return true;
                }
        }
        return false;
    }

    private void fastRemove(int index) {
        int numMoved = size - index - 1;
        if (numMoved > 0)
            System.arraycopy(elementData, index+1, elementData, index,
                    numMoved);
        elementData[--size] = null; // clear to let GC do its work
    }

    public boolean contains(T item) {
        return indexOf(item) >= 0;
    }

    public int indexOf(T item) {
        if (item == null) {
            for (int i = 0; i < size; i++)
                if (elementData[i]==null)
                    return i;
        } else {
            for (int i = 0; i < size; i++)
                if (item.equals(elementData[i]))
                    return i;
        }
        return -1;
    }

    public int getCapacity() {
        return calculateCapacity(elementData, elementData.length);
    }

    public void reverse() {
        Object[] reverseArray = Arrays.copyOf(elementData, elementData.length);
        for (int i = 0, j = size - 1; i < size; i++, j--){
            elementData[i] = reverseArray[j];
        }

    }

    public Object[] toArray() {
        Object[] cloneToArray = new Object[size];
        System.arraycopy(elementData, 0, cloneToArray, 0, size);
        return cloneToArray;
    }


    public void ensureCapacity(int minCapacity) {
        int minExpand = (elementData != DEFAULTCAPACITY_EMPTY_ELEMENTDATA)
                // any size if not default element table
                ? 0
                // larger than default for default empty table. It's already
                // supposed to be at default size.
                : DEFAULT_CAPACITY;

        if (minCapacity > minExpand) {
            ensureExplicitCapacity(minCapacity);
        }
    }

    private static int calculateCapacity(Object[] elementData, int minCapacity) {
        if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
            return Math.max(DEFAULT_CAPACITY, minCapacity);
        }
        return minCapacity;
    }

    private void ensureCapacityInternal(int minCapacity) {
        ensureExplicitCapacity(calculateCapacity(elementData, minCapacity));
    }

    private void ensureExplicitCapacity(int minCapacity) {
        // overflow-conscious code
        if (minCapacity - elementData.length > 0)
            grow(minCapacity);
    }

    private void grow(int minCapacity) {
        // overflow-conscious code
        int oldCapacity = elementData.length;
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        if (newCapacity - minCapacity < 0)
            newCapacity = minCapacity;
        if (newCapacity - MAX_ARRAY_SIZE > 0)
            newCapacity = hugeCapacity(minCapacity);
        // minCapacity is usually close to size, so this is a win:
        elementData = Arrays.copyOf(elementData, newCapacity);
    }

    private static int hugeCapacity(int minCapacity) {
        if (minCapacity < 0) // overflow
            throw new OutOfMemoryError();
        return (minCapacity > MAX_ARRAY_SIZE) ?
                Integer.MAX_VALUE :
                MAX_ARRAY_SIZE;
    }

    private void rangeCheckForAdd(int index) {
        if (index > size || index < 0)
            throw new ArrayIndexOutOfBoundsException("Index is out of bounds");
    }


    private void rangeCheck(int index) {
        if (index >= size)
            throw new ArrayIndexOutOfBoundsException("Index is out of bounds");
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < elementData.length; i++){
            if (elementData[i] != null) {
                result += elementData[i] + " ";
            }
        }
        return result;
    }
}
