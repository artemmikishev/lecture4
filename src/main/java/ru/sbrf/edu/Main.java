package ru.sbrf.edu;

public class Main {
    public static void main(String[] args) {
        CustomArrayImpl<String> list = new CustomArrayImpl<String>();
        System.out.println("Пустой список? " + list.isEmpty());
        list.add("0_Hello");
        list.add("1_Hello");
        list.add("2_Hello");
        list.add("3_Hello");
        list.add("4_Hello");
        list.add("5_Hello");
        list.add("6_Hello");
        list.add("7_Hello");
        list.add("8_Hello");
        list.add("9_Hello");
        System.out.println("Прямой порядок коллекции " + list);
        System.out.println("Пустой список? " + list.isEmpty());
        System.out.println("Размер? " + list.size());
        list.reverse();
        System.out.println("Обратный порядок коллекции " + list);

    }
}
